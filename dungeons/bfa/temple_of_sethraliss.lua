-- get the namespace
local _, namespace = ...;

namespace.dungeons.bfa["Temple of Sethraliss"] = {
	["Adderis and Aspix"] =
	{
		["Normal"] = {
			"- Attack the one that doesn't have [Lightning Shield] on them.",
			"- Spread out for [Arc Dash]"
		},
		["Heroic"] = {
			"- Attack the one that doesn't have [Lightning Shield] on them.",
			"- Spread out for [Arc Dash]"
		},
		["Mythic"] = {
			"- Attack the one that doesn't have [Lightning Shield] on them.",
			"- Spread out for [Arc Dash]"
		}
	},
	["Merektha"] =
	{
		["Normal"] = {
			"- Prioritize adds",
			"- Pull adds out of the dust so they can be attacked",
			"- Face away when the boss casts [Blinding Sand]",
			"- Don't stand in poison on the ground.",
			"- Avoid the boss when he [Burrow]s underground."
		},
		["Heroic"] = {
			"- Prioritize adds and [A Knot of Snakes]. You can CC [A Knot of Snakes] to remove it.",
			"- Pull adds out of the dust so they can be attacked",
			"- Face away when the boss casts [Blinding Sand]",
			"- Don't stand in poison on the ground.",
			"- Avoid the boss when he [Burrow]s underground."
		},
		["Mythic"] = {
			"- Prioritize adds and [A Knot of Snakes]. You can CC [A Knot of Snakes] to remove it.",
			"- Pull adds out of the dust so they can be attacked",
			"- Face away when the boss casts [Blinding Sand]",
			"- Don't stand in poison on the ground.",
			"- Avoid the boss when he [Burrow]s underground."
		}
	},
	["Galvazzt"] =
	{
		["Normal"] = {
			"- Look for the pillars that pop up. One person at a time needs to stand between each pillar and the boss to intercept the beam.",
			"- Don't move from your beam until the pillar goes away and don't run into another beam until your stacks of [Galvonized] go away from the last beam.",
			"- If beams hit the boss, he'll gain energy. He casts [Consume Charge] at 100 energy.",
			"- If you see the boss casting [Consume Charge], pop defensive cooldowns."
		},
		["Heroic"] = {
			"- Look for the pillars that pop up. One person at a time needs to stand between each pillar and the boss to intercept the beam.",
			"- Don't move from your beam until the pillar goes away and don't run into another beam until your stacks of [Galvonized] go away from the last beam.",
			"- If beams hit the boss, he'll gain energy. He casts [Consume Charge] at 100 energy.",
			"- If you see the boss casting [Consume Charge], pop defensive cooldowns."
		},
		["Mythic"] = {
			"- Look for the pillars that pop up. One person at a time needs to stand between each pillar and the boss to intercept the beam.",
			"- Don't move from your beam until the pillar goes away and don't run into another beam until your stacks of [Galvonized] go away from the last beam.",
			"- If beams hit the boss, he'll gain energy. He casts [Consume Charge] at 100 energy.",
			"- If you see the boss casting [Consume Charge], pop defensive cooldowns."
		}
	},
	["Avatar of Sethraliss"] =
	{
		["Normal"] = {
			"- Prioritize killing the adds in this order: Heart Guardian > Hoodo Hexer. Interrupt the Plague Doctors when you can.",
			"- Heal the Avatar.",
			"- Step on the toads if you're not healing.",
		},
		["Heroic"] = {
			"- Prioritize killing the adds in this order: Heart Guardian > Plague Doctor > Hoodo Hexer. Interrupt the Plague Doctors when you can.",
			"- Heal the Avatar.",
			"- Dispel players incapacitated by [Snake Charm].",
			"- Step on the toads if you're not healing.",
		},
		["Mythic"] = {
			"- Prioritize killing the adds in this order: Heart Guardian > Plague Doctor > Hoodo Hexer. Interrupt the Plague Doctors when you can.",
			"- Heal the Avatar.",
			"- Dispel players incapacitated by [Snake Charm].",
			"- Step on the toads if you're not healing.",
		}
	},
	
	
	
}