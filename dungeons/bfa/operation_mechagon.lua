-- get the namespace
local _, namespace = ...;

-- DOES NOT HAVE NORMAL OR HEROIC (MYTHIC ONLY)
namespace.dungeons.bfa["Operation: Mechagon"] = {
	["King Gobbamak"] =
	{
		["Normal"] = {
			"THIS BOSS DOES NOT HAVE A NORMAL DIFFICULTY.",
		},
		["Heroic"] = {
			"When the boss reaches 100 energy, stack on the tank for [Charged Smash].",
			"After [Charged Smash], each player should activate a pylon. This will take care of the adds."
		},
		["Mythic"] = {
			"When the boss reaches 100 energy, stack on the tank for [Charged Smash].",
			"After [Charged Smash], each player should activate a pylon. This will take care of the adds."
		}
	},
	["Gunker"] =
	{
		["Normal"] = {
			"THIS BOSS DOES NOT HAVE A NORMAL DIFFICULTY.",
		},
		["Heroic"] = {
			"Avoid the green stuff on the floor",
			"Stay near the squirt bots, they clean up the green stuff."
		},
		["Mythic"] = {
			"Avoid the green stuff on the floor",
			"Stay near the squirt bots, they clean up the green stuff."
		}
	},
	["Trixie & Naeno"] =
	{
		["Normal"] = {
			"THIS BOSS DOES NOT HAVE A NORMAL DIFFICULTY.",
		},
		["Heroic"] = {
			"Aim to kill them at about the same time. When one dies, the other gets a big buff.",
			"Avoid the bike charge and stay somewhat spread out.",
			"Stand in the exhaust smoke if targeted by [Mega Taze].",
		},
		["Mythic"] = {
			"Aim to kill them at about the same time. When one dies, the other gets a big buff.",
			"Avoid the bike charge and stay somewhat spread out.",
			"Stand in the exhaust smoke if targeted by [Mega Taze].",
		}
	},
	["HK-8 Aerial Oppression Unit"] =
	{
		["Normal"] = {
			"THIS BOSS DOES NOT HAVE A NORMAL DIFFICULTY.",
		},
		["Heroic"] = {
			"Tank: Use defensive cooldowns when the boss uses [Wreck].",
			"Kite/snare the Walkie Shockie adds.",
			"Once you kill Tank Buster MK1, run up to the top platform without getting hit by the bots.",
			"When the HK-8 Aerial Oppression Unit crashes, go back down and dps it. Avoid the orange circle when it starts back up.",
			"Repeat the above until the HK-8 Aerial Oppression Unit is defeated."
		},
		["Mythic"] = {
			"Tank: Use defensive cooldowns when the boss uses [Wreck].",
			"Kite/snare the Walkie Shockie adds.",
			"Once you kill Tank Buster MK1, run up to the top platform without getting hit by the bots.",
			"When the HK-8 Aerial Oppression Unit crashes, go back down and dps it. Avoid the orange circle when it starts back up.",
			"Repeat the above until the HK-8 Aerial Oppression Unit is defeated."
		}
	},
	["Tussle Tonks"] =
	{
		["Normal"] = {
			"THIS BOSS DOES NOT HAVE A NORMAL DIFFICULTY.",
		},
		["Heroic"] = {
			"Dodge all of the traps and avoid the bombs.",
			"Bring the Platinum Pummeler to the side of the room near a hammer. Make sure the Platinum Pummeler gets hit by the hammer. Do this 3 times to deal full damage.",
			"Dodge the Gnomercy 4 U charge so it rams into the wall."
		},
		["Mythic"] = {
			"Dodge all of the traps and avoid the bombs.",
			"Bring the Platinum Pummeler to the side of the room near a hammer. Make sure the Platinum Pummeler gets hit by the hammer. Do this 3 times to deal full damage.",
			"Dodge the Gnomercy 4 U charge so it rams into the wall."
		}
	},
	["K.U.-J.0."] =
	{
		["Normal"] = {
			"THIS BOSS DOES NOT HAVE A NORMAL DIFFICULTY.",
		},
		["Heroic"] = {
			"LOS [Venting Flames] by getting behind a cube. Use the newest cube each time.",
			"Dispell [Blazing Chomp] around 2-3 stacks depending on what the tank can handle.",
		},
		["Mythic"] = {
			"LOS [Venting Flames] by getting behind a cube. Use the newest cube each time.",
			"Dispell [Blazing Chomp] around 2-3 stacks depending on what the tank can handle.",
		}
	},
	["Machinist's Garden"] =
	{
		["Normal"] = {
			"THIS BOSS DOES NOT HAVE A NORMAL DIFFICULTY.",
		},
		["Heroic"] = {
			"Kill the Inconspicuous Plant adds.",
			"Dodge the spinning gears and fire beams that come out from the middle of the room."
		},
		["Mythic"] = {
			"Kill the Inconspicuous Plant adds.",
			"Dodge the spinning gears and fire beams that come out from the middle of the room."
		}
	},
	["King Mechagon"] =
	{
		["Normal"] = {
			"THIS BOSS DOES NOT HAVE A NORMAL DIFFICULTY.",
		},
		["Heroic"] = {
			"Dodge the plasma orbs.",
			"Spread out and don't stand behind anyone so that [Giga Zap] only ever hits one player.",
			"Run away from the boss if targeted by [Cutting Beam]"
		},
		["Mythic"] = {
			"Dodge the plasma orbs.",
			"Spread out and don't stand behind anyone so that [Giga Zap] only ever hits one player.",
			"Run away from the boss if targeted by [Cutting Beam]"
		}
	},
	
}