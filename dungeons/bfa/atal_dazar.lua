-- get the namespace
local _, namespace = ...;

namespace.dungeons.bfa["Atal'Dazar"] = {
	["Priestess Alun'za"] =
	{
		["Normal"] = {
			"- Stand in the red circles when she starts casting [Transfusion].",
		},
		["Heroic"] = {
			"- Stand in the red circles when she starts casting [Transfusion].",
			"- Prioritize the [Spirit of Gold] adds. They need to go down quick or they'll absorb the red circles.",
			"- Avoid the moving golden orbs."
		},
		["Mythic"] = {
			"- Stand in the red circles when she starts casting [Transfusion].",
			"- Prioritize the [Spirit of Gold] adds. They need to go down quick or they'll absorb the red circles.",
			"- Avoid the moving golden orbs."
		}
	},
	["Vol'kaal"] =
	{
		["Normal"] = {
			"- Phase 1: The three totems need to go down at the same time.",
			"- Phase 2: Avoid standing in [Toxic Pools]. Interrupt [Noxious Stench]."
		},
		["Heroic"] = {
			"- Phase 1: The three totems need to go down at the same time.",
			"- Phase 2: Avoid standing in [Toxic Pools]. Interrupt [Noxious Stench]."
		},
		["Mythic"] = {
			"- Phase 1: The three totems need to go down at the same time.",
			"- Phase 2: Avoid standing in [Toxic Pools]. Interrupt [Noxious Stench]."
		}
	},
	["Rezan"] =
	{
		["Normal"] = {
			"- Avoid stepping on the purple spots on the ground. They spawn adds. If the boss steps on the purple spots, this will also spawn the adds.",
			"- When he casts [Terrifying Visage], run out of his line of sight (behind the stairs).",
			"- If you're targeted by [Pursue], run away from the boss. A good path to run from [Pursue] to avoid spawning adds is through the water."
		},
		["Heroic"] = {
			"- Avoid stepping on the purple spots on the ground. They spawn adds. If the boss steps on the purple spots, this will also spawn the adds.",
			"- When he casts [Terrifying Visage], run out of his line of sight (behind the stairs).",
			"- If you're targeted by [Pursue], run away from the boss. A good path to run from [Pursue] to avoid spawning adds is through the water."
		},
		["Mythic"] = {
			"- Avoid stepping on the purple spots on the ground. They spawn adds. If the boss steps on the purple spots, this will also spawn the adds.",
			"- When he casts [Terrifying Visage], run out of his line of sight (behind the stairs).",
			"- If you're targeted by [Pursue], run away from the boss. A good path to run from [Pursue] to avoid spawning adds is through the water."
		}
	},
	["Yazma"] =
	{
		["Normal"] = {
			"- When you're targeted with [Soulrend], move away from the boss. Then kill the [Soulspawn]s before they reach the boss. They can be stunned/snared/slowed.",
			"- Avoid standing on the spiders."
		},
		["Heroic"] = {
			"- When you're targeted with [Soulrend], move away from the boss. Then kill the [Soulspawn]s before they reach the boss. They can be stunned/snared/slowed.",
			"- Avoid standing on the spiders."
		},
		["Mythic"] = {
			"- When you're targeted with [Soulrend], move away from the boss. Then kill the [Soulspawn]s before they reach the boss. They can be stunned/snared/slowed.",
			"- Avoid standing on the spiders."
		}
	}
}