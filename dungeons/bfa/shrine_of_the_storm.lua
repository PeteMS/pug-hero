-- get the namespace
local _, namespace = ...;

namespace.dungeons.bfa["Shrine of the Storm"] = {
	["Aqu'sirr"] =
	{
		["Normal"] = {
			"- Don't stand in front of the boss when he casts [Surging Rush].",
			"- If you're targeted by [Undertow], don't let it knock you off the platform.",
			"- Cleanse [Choking Brine]. This will cause blue swirls to pop up around the player cleansed. Don't stand in them.",
			"- At 50% health the boss splits into 3 adds.",
			"- Make sure at least one person is standing in meele range of each add at all times."
		},
		["Heroic"] = {
			"- Prioritize killing tentacles that are rooting players.",
			"- Don't stand in front of the boss when he casts [Surging Rush].",
			"- If you're targeted by [Undertow], don't let it knock you off the platform.",
			"- Cleanse [Choking Brine]. This will cause blue swirls to pop up around the player cleansed. Don't stand in them.",
			"- At 50% health the boss splits into 3 adds.",
			"- Make sure at least one person is standing in meele range of each add at all times."
		},
		["Mythic"] = {
			"- Prioritize killing tentacles that are rooting players.",
			"- Don't stand in front of the boss when he casts [Surging Rush].",
			"- If you're targeted by [Undertow], don't let it knock you off the platform.",
			"- Cleanse [Choking Brine]. This will cause blue swirls to pop up around the player cleansed. Don't stand in them.",
			"- At 50% health the boss splits into 3 adds.",
			"- Make sure at least one person is standing in meele range of each add at all times."
		}
	},
	["Tidesage Council"] =
	{
		["Normal"] = {
			"- Interrupt [Slicing Blast] as much as possible.",
			"- Stand in [Reinforcing Ward] to remove debuffs on yourself, but keep the bosses out of it.",
			"- Stand in [Swiftness Ward] to get a haste buff, but keep the bosses out of it.",
			"- Tank: Face Brother Ironhull away from the group so that [Hindering Cleave] doesn't hit anyone.",
		},
		["Heroic"] = {
			"- Interrupt [Slicing Blast] as much as possible.",
			"- Stand in [Reinforcing Ward] to remove debuffs on yourself, but keep the bosses out of it.",
			"- Stand in [Swiftness Ward] to get a haste buff, but keep the bosses out of it.",
			"- Avoid the tornadoes.",
			"- Tank: Face Brother Ironhull away from the group so that [Hindering Cleave] doesn't hit anyone.",
			"- Tank: Kite Brother Ironhull when he's buffed by [Blessing of Ironsides]."
		},
		["Mythic"] = {
			"- Interrupt [Slicing Blast] as much as possible.",
			"- Stand in [Reinforcing Ward] to remove debuffs on yourself, but keep the bosses out of it.",
			"- Stand in [Swiftness Ward] to get a haste buff, but keep the bosses out of it.",
			"- Avoid the tornadoes.",
			"- Tank: Face Brother Ironhull away from the group so that [Hindering Cleave] doesn't hit anyone.",
			"- Tank: Kite Brother Ironhull when he's buffed by [Blessing of Ironsides]."
		}
	},
	["Lord Stormsong"] =
	{
		["Normal"] = {
			"- Interrupt [Void Bolt] as much as possible.",
			"- Kite the orbs around or pop a strong defensive and run into them.",
			"- If you're targeted by the [Ancient Mindbender], run into a bunch of orbs until it falls off you. It falls off when you hit 50% health."
		},
		["Heroic"] = {
			"- Interrupt [Void Bolt] as much as possible.",
			"- Dispell [Mind Rend] as much as possible.",
			"- Kite the orbs around or pop a strong defensive and run into them.",
			"- If you're targeted by the [Ancient Mindbender], run into a bunch of orbs until it falls off you. It falls off when you hit 50% health."
		},
		["Mythic"] = {
			"- Interrupt [Void Bolt] as much as possible.",
			"- Dispell [Mind Rend] as much as possible.",
			"- Kite the orbs around or pop a strong defensive and run into them.",
			"- If you're targeted by the [Ancient Mindbender], run into a bunch of orbs until it falls off you. It falls off when you hit 50% health."
		}
	},
	["Vol'zith the Whisperer"] =
	{
		["Normal"] = {
			"- Phase 1:",
			"- Run away from [Yawning Gate] and don't stand in the big black/purple spots it leaves behind.",
			"- Avoid the tentacle slams.",
			"- Focus down adds and don't let them get to the boss. These only come out after the first phase 2.",
			"- Healer: [Whispers of Power] buffs the players affected by it. Dispell [Whispers of Power] when there are too many stacks for you to keep up with. Otherwise, leave it so the player gets the buff.",
			"- Phase 2 (Everyone gets phased and adds come out):",
			"- Tank: Pick up and kite the Sunken Denizen add. Kite it if you're taking too much damage.",
			"- Interrupt the boss and it's back to phase 1."
		},
		["Heroic"] = {
			"- Phase 1:",
			"- Run away from [Yawning Gate] and don't stand in the big black/purple spots it leaves behind.",
			"- Avoid the tentacle slams.",
			"- Focus down adds and don't let them get to the boss. These only come out after the first phase 2.",
			"- Healer: [Whispers of Power] buffs the players affected by it. Dispell [Whispers of Power] when there are too many stacks for you to keep up with. Otherwise, leave it so the player gets the buff.",
			"- Phase 2 (Everyone gets phased and adds come out):",
			"- DPS: Interrupt the Forgotten Denizen adds and nuke them down.",
			"- Tank: Pick up and kite the Sunken Denizen add. Kite it if you're taking too much damage.",
			"- Once the adds are killed. Interrupt the boss and it's back to phase 1."
		},
		["Mythic"] = {
			"- Phase 1:",
			"- Run away from [Yawning Gate] and don't stand in the big black/purple spots it leaves behind.",
			"- Avoid the tentacle slams.",
			"- Focus down adds and don't let them get to the boss. These only come out after the first phase 2.",
			"- Healer: [Whispers of Power] buffs the players affected by it. Dispell [Whispers of Power] when there are too many stacks for you to keep up with. Otherwise, leave it so the player gets the buff.",
			"- Phase 2 (Everyone gets phased and adds come out):",
			"- DPS: Interrupt the Forgotten Denizen adds and nuke them down.",
			"- Tank: Pick up and kite the Sunken Denizen add. Kite it if you're taking too much damage.",
			"- Once the adds are killed. Interrupt the boss and it's back to phase 1."
		}
	},
	
	
	
}