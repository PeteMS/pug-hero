-- get the namespace
local _, namespace = ...;

namespace.dungeons.bfa["Waycrest Manor"] = {
	["Heartsbane Triad"] =
	{
		["Normal"] = {
			"- Attack the one with [Focusing Iris] (the big one).",
			"- Attack players affected by [Soul Manipulation].",
			"- Run away from the group when you are targeted with [Unstable Runic Mark]"	
		},
		["Heroic"] = {
			"- Attack the one with [Focusing Iris] (the big one).",
			"- Attack players affected by [Soul Manipulation].",
			"- Run away from the group when you are targeted with [Unstable Runic Mark]"
		},
		["Mythic"] = {
			"- Attack the one with [Focusing Iris] (the big one).",
			"- Attack players affected by [Soul Manipulation].",
			"- Run away from the group when you are targeted with [Unstable Runic Mark]"
		}
	},
	["Soulbound Goliath"] =
	{
		["Normal"] = {
			"- Prioritize destroying [Soul Thorns] to free party members.",
			"- Tank: Move the boss into [Wildfire] (patches of fire) occasionally to reduce its damage increase."
		},
		["Heroic"] = {
			"- Prioritize destroying [Soul Thorns] to free party members.",
			"- Tank: Move the boss into [Wildfire] (patches of fire) occasionally to reduce its damage increase.",
			"- Avoid the adds until they despawn."
		},
		["Mythic"] = {
			"- Prioritize destroying [Soul Thorns] to free party members.",
			"- Tank: Move the boss into [Wildfire] (patches of fire) occasionally to reduce its damage increase.",
			"- Avoid the adds until they despawn."
		}
	},
	["Raal the Gluttonous"] =
	{
		["Normal"] = {
			"- Avoid [Rotten Expulsion].",
			"- Prioritize killing Wasting Servants summoned by [Call Servant]."
		},
		["Heroic"] = {
			"- Avoid [Rotten Expulsion].",
			"- Prioritize killing Wasting Servants summoned by [Call Servant]."

		},
		["Mythic"] = {
			"- Avoid [Rotten Expulsion].",
			"- Prioritize killing Wasting Servants summoned by [Call Servant]."

		}
	},
	["Lord and Lady Waycrest"] =
	{
		["Normal"] = {
			"- If targeted with [Virulent Pathogen] run out of the group.",
			"- Avoid [Discordant Cadenza]."
		},
		["Heroic"] = {
			"- If targeted with [Virulent Pathogen] run out of the group.",
			"- Avoid [Discordant Cadenza]."
		},
		["Mythic"] = {
			"- If targeted with [Virulent Pathogen] run out of the group.",
			"- Avoid [Discordant Cadenza]."
		}
	},
	["Gorak Tul"] =
	{
		["Normal"] = {
			"- Prioritize killing the adds (Deathtouched Slavers).",
			"- Use [Alchemical Fire] to destroy the Deathtouched Slaver corpses."
		},
		["Heroic"] = {
			"- Prioritize killing the adds (Deathtouched Slavers).",
			"- Use [Alchemical Fire] to destroy the Deathtouched Slaver corpses."
		},
		["Mythic"] = {
			"- Prioritize killing the adds (Deathtouched Slavers).",
			"- Use [Alchemical Fire] to destroy the Deathtouched Slaver corpses."
		}
	},
	
	
	
}