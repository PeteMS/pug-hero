-- get the namespace
local _, namespace = ...;

namespace.dungeons.bfa["Tol Dagor"] = {
	["The Sand Queen"] =
	{
		["Normal"] = {
			"- Avoid the [Sand Traps] on the ground.",
			"- Avoid [Upheaval] when the boss is coming back up from underground"
		},
		["Heroic"] = {
			"- Avoid the [Sand Traps] on the ground.",
			"- Avoid [Upheaval] when the boss is coming back up from underground"
		},
		["Mythic"] = {
			"- Avoid the [Sand Traps] on the ground.",
			"- Avoid [Upheaval] when the boss is coming back up from underground"
		}
	},
	["Jes Howlis"] =
	{
		["Normal"] = {
			"- Get behind a pillar to LOS [Flashing Daggers]",
			"- Interrupt [Howling Fear]",
			"- At 50% health he opens all of the cells on that floor to let the prisoners out.",
			"- If there's a rogue in the group, you can open them yourself before the fight and kill the prisoners ahead of time."
		},
		["Heroic"] = {
			"- Get behind a pillar to LOS [Flashing Daggers]",
			"- Interrupt [Howling Fear]",
			"- At 50% health he opens all of the cells on that floor to let the prisoners out.",
			"- If there's a rogue in the group, you can open them yourself before the fight and kill the prisoners ahead of time."
		},
		["Mythic"] = {
			"- Get behind a pillar to LOS [Flashing Daggers]",
			"- Interrupt [Howling Fear]",
			"- At 50% health he opens all of the cells on that floor to let the prisoners out.",
			"- If there's a rogue in the group, you can open them yourself before the fight and kill the prisoners ahead of time."
		}
	},
	["Knight Captain Valyri"] =
	{
		["Normal"] = {
			"- Top Priority is to move munition piles away from [Ignition] and [Cinderflame].",
			"- Don't touch munition piles if you're afflicted by [Fuselighter].",
			"- Don't stand in the fire."
		},
		["Heroic"] = {
			"- Top Priority is to move munition piles away from [Ignition] and [Cinderflame].",
			"- Don't touch munition piles if you're afflicted by [Fuselighter].",
			"- Don't stand in the fire."
		},
		["Mythic"] = {
			"- Top Priority is to move munition piles away from [Ignition] and [Cinderflame].",
			"- Don't touch munition piles if you're afflicted by [Fuselighter].",
			"- Don't stand in the fire."
		}
	},
	["Overseer Korgus"] =
	{
		["Normal"] = {
			"- Pay attention to the prison cannons off to the side and avoid them when they go off.",
			"- Rotate out who gets hit by [Deadeye].",
			"- Stay an appropriate distance away from other players if you get targeted with [Azerite Rounds: Blast].",
			"- Avoid Crossfire. The ground will turn a shade of black."
		},
		["Heroic"] = {
			"- Pay attention to the meter that pops up during the fight. When you move, it fills up. If it fills up completely you'll get stunned.",
			"- Pay attention to the prison cannons off to the side and avoid them when they go off.",
			"- Rotate out who gets hit by [Deadeye].",
			"- Stay an appropriate distance away from other players if you get targeted with [Azerite Rounds: Blast].",
			"- Avoid Crossfire. The ground will turn a shade of black."
		},
		["Mythic"] = {
			"- Pay attention to the meter that pops up during the fight. When you move, it fills up. If it fills up completely you'll get stunned.",
			"- Pay attention to the prison cannons off to the side and avoid them when they go off.",
			"- Rotate out who gets hit by [Deadeye].",
			"- Stay an appropriate distance away from other players if you get targeted with [Azerite Rounds: Blast].",
			"- Avoid Crossfire. The ground will turn a shade of black."
		}
	},
	
	
	
}