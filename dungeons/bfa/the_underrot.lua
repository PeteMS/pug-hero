-- get the namespace
local _, namespace = ...;

namespace.dungeons.bfa["The Underrot"] = {
	["Elder Leaxa"] =
	{
		["Normal"] = {
			"- Prioritize the Blood Effigies created by [Blood Mirror].",
			"- Avoid [Creeping Rot] (green puddle on ground)."
		},
		["Heroic"] = {
			"- Prioritize the Blood Effigies created by [Blood Mirror].",
			"- Avoid [Creeping Rot] (green puddle on ground)."
		},
		["Mythic"] = {
			"- Prioritize the Blood Effigies created by [Blood Mirror].",
			"- Avoid [Creeping Rot] (green puddle on ground)."
		}
	},
	["Cragmaw the Infested"] =
	{
		["Normal"] = {
			"- Clear all trash before engaging the boss.",
			"- Avoid charge and step on the Blood Ticks when they pop up."
		},
		["Heroic"] = {
			"- Clear all trash before engaging the boss.",
			"- Avoid charge and step on the Blood Ticks when they pop up."
		},
		["Mythic"] = {
			"- Clear all trash before engaging the boss.",
			"- Avoid charge and step on the Blood Ticks when they pop up."
		}
	},
	["Sporecaller Zancha"] =
	{
		["Normal"] = {
			"- If you're targeted by [259718], run next to the spores.",
			"- Tank: Position the boss so that [Shockwave] destroys the spores.",
			"- If spores still remain when the boss is casting [Festering Harvest], pop a defensive ability and run through the spores to clear them."
		},
		["Heroic"] = {
			"- If you're targeted by [259718], run next to the spores.",
			"- Tank: Position the boss so that [Shockwave] destroys the spores.",
			"- If spores still remain when the boss is casting [Festering Harvest], pop a defensive ability and run through the spores to clear them."
		},
		["Mythic"] = {
			"- If you're targeted by [259718], run next to the spores.",
			"- Tank: Position the boss so that [Shockwave] destroys the spores.",
			"- If spores still remain when the boss is casting [Festering Harvest], pop a defensive ability and run through the spores to clear them."
		}
	},
	["Unbound Abomination"] =
	{
		["Normal"] = {
			"- Prioritize the Blood Visages adds",
			"- Don't stand in corruption spots",
			"- If you're targeted with [Cleansing Light], stand near corruption spots to purge them.",
			"- The rest of the group should stack on the player targeted with [Cleansing Light] to remove [Putrid Blood] stacks.",
			"- Healer: dispell [Putrid Blood]"
		},
		["Heroic"] = {
			"- Prioritize the Blood Visages adds",
			"- Don't stand in corruption spots",
			"- If you're targeted with [Cleansing Light], stand near corruption spots to purge them.",
			"- The rest of the group should stack on the player targeted with [Cleansing Light] to remove [Putrid Blood] stacks.",
			"- Healer: dispell [Putrid Blood]"
		},
		["Mythic"] = {
			"- Prioritize the Blood Visages adds",
			"- Don't stand in corruption spots",
			"- If you're targeted with [Cleansing Light], stand near corruption spots to purge them.",
			"- The rest of the group should stack on the player targeted with [Cleansing Light] to remove [Putrid Blood] stacks.",
			"- Healer: dispell [Putrid Blood]"
		}
	},
	
	
	
}