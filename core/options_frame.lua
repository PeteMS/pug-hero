-- get the namespace
local _, namespace = ...;

-- Title of options window
PUGHERO_OPTIONS_MENU_TITLE = "PUG Hero"

-- namespace methods
namespace.options = {

	-- show or hide the options window depending on if it's already shown
	show = function()
		if ( PUGHeroOptionsFrame:IsShown() ) then
			PUGHeroOptionsFrame:Hide();
		else
			PUGHeroOptionsFrame:Show();
		end
	end,

}

-- function triggered when options window is shown
function PUGHeroOptionsFrame_OnShow()
	--Refresh the two category lists and display the "Controls" group of options if nothing is selected.
	PUGHeroCategoryList_Update();
	PUGHeroOptionsFrame_RefreshCategories();
end

-- function triggered when options window is hidden
function PUGHeroOptionsFrame_OnHide()
	OptionsFrame_OnHide(PUGHeroOptionsFrame);
end

-- function triggered when options window is loaded
function PUGHeroOptionsFrame_OnLoad()
	-- uncomment to show options on startup for debugging
	-- namespace.options.show()
end

-- Close button click
function PUGHeroOptionsFrameClose_OnClick()
	namespace.options.show()
end

-- Defaults button click
function PUGHeroOptionsFrameDefaults_OnClick()
	
	StaticPopupDialogs["EXAMPLE_HELLOWORLD"] = {
	  text = "This will restore all defaults for PUG Hero.\nThis will also reboot the UI.\nContinue?",
	  button1 = "Yes",
	  button2 = "No",
	  OnAccept = function()
		  PUGHero_ReloadAndRestoreDefaults()
	  end,
	  timeout = 0,
	  whileDead = true,
	  hideOnEscape = true,
	  preferredIndex = 3,  -- avoid some UI taint, see http://www.wowace.com/announcements/how-to-avoid-some-ui-taint/
	}
	
	StaticPopup_Show("EXAMPLE_HELLOWORLD")
	
end

function PUGHero_ReloadAndRestoreDefaults()

	PUGHeroDB.restoreDefaults = true;
	ReloadUI();

end


