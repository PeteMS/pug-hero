-- get the namespace
local _, namespace = ...;

PUGHero = {}

local frame = CreateFrame("Frame")

-- trigger event with /reloadui or /rl
frame:RegisterEvent("PLAYER_LOGIN")

frame:SetScript("OnEvent", function(this, event, ...)
	PUGHero[event](PUGHero, ...)
end)

function PUGHero:PLAYER_LOGIN()

	-- set defaults if the saved variable doesn't exist
	self:SetDefaults()
	
	-- put the db in the namespace
	namespace.PUGHeroDB = PUGHeroDB;

end

function PUGHero:SetDefaults()

	-- if we're restoring defaults, then set this to nil so we initialize with defaults
	if PUGHeroDB and PUGHeroDB.restoreDefaults then
		PUGHeroDB = nil
	end
	
	-- initialize PUGHeroDB if it doesn't exist or if we're restoring defaults
	if not PUGHeroDB then
	
		PUGHeroDB = {
			chatChannel = "SELF",
			dungeons = namespace.dungeons,
			raids = namespace.raids,
			restoreDefaults = false
		}
		
		print("PUG Hero initialized with default values")
	
	end

end

-- gets a list of bosses based on the instance type, expansion, and instance
local function PUGHero_GetBosses(instanceType, expansion, instance)

	local bosses = nil
	-- Vanilla Dungeon
	if (expansion == "Vanilla" and instanceType == "Dungeons") then
		bosses = namespace.PUGHeroDB.dungeons.vanilla[instance]
	-- BFA Dungeon
	elseif (expansion == "Battle for Azeroth" and instanceType == "Dungeons") then
		bosses = namespace.PUGHeroDB.dungeons.bfa[instance]
	-- BFA Raid
	elseif (expansion == "Battle for Azeroth" and instanceType == "Raids") then
		bosses = namespace.PUGHeroDB.raids.bfa[instance]
	end
	
	return bosses
	
end

-- get saved variable for the expansion, instance, boss, and difficulty
function PUGHero_getExplanation(instanceType, expansion, instance, boss, difficulty)

	-- get bosses
	local bosses = PUGHero_GetBosses(instanceType, expansion, instance)
	
	-- find the explanation for the specific boss and difficulty
	local explanations = bosses[boss]
	return explanations[difficulty]

end

-- get saved variable for the expansion, instance, boss, and difficulty
function PUGHero_setExplanation(instanceType, expansion, instance, boss, difficulty, explanation)

	-- get bosses
	local bosses = PUGHero_GetBosses(instanceType, expansion, instance)
	
	-- find the explanation for the specific boss and difficulty
	local explanations = bosses[boss]
	explanations[difficulty] = explanation

end