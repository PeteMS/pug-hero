-- get the namespace
local _, namespace = ...;

-- probably want to eventually convert everything to go by id and get rid of this.
-- spell and dungeon/raid mappings are done in the dungeon/raid files
namespace.mappings = {}

-- difficulty mappings
namespace.mappings[1] = "Normal"
namespace.mappings[2] = "Heroic"
namespace.mappings[8] = "Mythic"
namespace.mappings[14] = "Normal"
namespace.mappings[15] = "Heroic"
namespace.mappings[16] = "Mythic"
namespace.mappings[17] = "Looking For Raid"
namespace.mappings[23] = "Mythic"
