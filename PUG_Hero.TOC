## Interface: 80300
## Title: PUG Hero
## Author: Sekai
## Version: 1.0.2
## Notes: A simple way of explaining fights to your dungeon or raid group
## DefaultState: enabled
## SavedVariables: PUGHeroDB
#@no-lib-strip@
Libs\libs.xml
#@end-no-lib-strip@

core/mappings.lua

dungeons/dungeons.lua
dungeons/bfa/waycrest_manor.lua
dungeons/bfa/tol_dagor.lua
dungeons/bfa/the_underrot.lua
dungeons/bfa/the_motherlode.lua
dungeons/bfa/temple_of_sethraliss.lua
dungeons/bfa/siege_of_boralus.lua
dungeons/bfa/shrine_of_the_storm.lua
dungeons/bfa/operation_mechagon.lua
dungeons/bfa/kings_rest.lua
dungeons/bfa/freehold.lua
dungeons/bfa/atal_dazar.lua

raids/raids.lua
raids/bfa/nyalotha_the_waking_city.lua

core/saved_variables_handler.lua
core/pug_hero_options.xml
core/chat_handler.lua
core/main.lua
